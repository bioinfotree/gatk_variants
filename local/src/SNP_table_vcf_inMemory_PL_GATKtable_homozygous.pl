#!/usr/bin/perl
#######################################################
# Description: takes a multisample vcf file with SNPs #
#              produces a table of genotypes          #
#######################################################
#chr1    576361  .       T       .       81.16   PASS    AC=0;AF=0.00;AN=2;DP=17;MQ=60.00;MQ0=0  GT:DP:GQ:PL     0/0:17:51.17:0,51,680
#chr1    576362  .       G       A       358.90  PASS    AC=1;AF=0.50;AN=2;BaseQRankSum=0.754;DP=17;Dels=0.00;FS=0.000;HRun=2;HaplotypeScore=0.0000;MQ=60.00;MQ0=0;MQRankSum=-0.653;QD=21.11;ReadPosRankSum=0.854        GT:AD:DP:GQ:PL  0/1:6,11:17:99:389,0,189
#chr1    576363  .       A       .       81.16   PASS    AC=0;AF=0.00;AN=2;DP=17;MQ=60.00;MQ0=0  GT:DP:GQ:PL     0/0:17:51.17:0,51,680
#chr1    576364  .       A       .       81.16   PASS    AC=0;AF=0.00;AN=2;DP=17;MQ=60.00;MQ0=0  GT:DP:GQ:PL     0/0:17:51.17:0,51,699
#chr1    576365  .       G       .       81.16   PASS    AC=0;AF=0.00;AN=2;DP=17;MQ=60.00;MQ0=0  GT:DP:GQ:PL     0/0:17:51.17:0,51,697

use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;
use Switch;
use Bio::DB::BigWig 'binMean';
use POSIX;

my $GATK; # multi sample input vcf from GATK
my $minVarieties = 2; # minimum number of varieties showing a SNP in a position to be considered
my $minSNP = 1; # minimum number of varieties exhibting a SNP
my $coverage; # file with coverage for each variety (check it against DP flag)
my $minCoverageRatio = 0.5; # minimum coverage compared to average coverage, default 0.5
my $maxCoverageRatio = 1.5; # maximum coverage compared to average coverage, default 1.5
my $minCoverage = 5; # minimum coverage, regardless of the individual average coverage
my $maxGenotypes = 2; # maximum genotypes in a single locus (AN flag)
my $header = 0; # keep vcf headers
my $PASS = 0; # keep SNPs with PASS quality on
my $AlleleFrequency = 0.01; # AF flag
my $QUAL = 0; # quality score
my $AD_on_DP_percentage = 0.8; # minimum ratio between AD and DP values
my $minimumPL = 30; # minimum value of PL for non zero values (meaning it should be unlikely to have multiple PLs with low values)
my $MVF = 0.2; # minimum variant frequency for a single sample call
my $help    = 0;

GetOptions(
	'GATK=s' => \$GATK,
	'minVarieties=i' => \$minVarieties,
	'minSNP=i' => \$minSNP,
	'coverage=s' => \$coverage,
	'minCoverageRatio=s' => \$minCoverageRatio,
	'maxCoverageRatio=s' => \$maxCoverageRatio,
	'minCoverage=s' => \$minCoverage,
	'maxGenotypes=i' => \$maxGenotypes,
	'header=i' => \$header,
	'PASS=i' => \$PASS,
	'AlleleFrequency=f' => \$AlleleFrequency,
	'QUAL=i' => \$QUAL,
	'AD_on_DP_percentage=f' => \$AD_on_DP_percentage,
	'minimumPL=i' => \$minimumPL,
	'MVF=f' => \$MVF,
	'help|?' => \$help
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(1) if (!defined($GATK) or !defined($coverage));

#my $command = "ls ".$DirSNP."/*.vcf";
#my @SNPfile = `$command`;
my @varieties;

# parse min coverage values
open(COVERAGE, $coverage) or die("Cannot open file with coverage for each variety\n");
my %minCoverage; # SNP min coverage for each variety
my %maxCoverage; # SNP max coverage for each variety
while (my $line = <COVERAGE>) {
	chomp $line;
	$line =~/(\S+)\t(\d+)/;
	my $variety = $1;
	my $min = ceil($2 * $minCoverageRatio);
	my $max = floor($2 * $maxCoverageRatio);
	$minCoverage{$variety} = $min;
	$maxCoverage{$variety} = $max;
	push (@varieties, $variety);
}
close COVERAGE;

print "# Sequence\tPosition\tReference\t";
for (my $i=0; $i < @varieties; $i++) {
	print $varieties[$i]."\t";
}
print "\n";

# parse SNP information only
open(GATK, $GATK) or die("Cannot open input file ".$GATK."\n");
my %SNP;
# print vcf headers
if ($header) {
	while (my $line = <$GATK>) {
		if ($line =~ /^\#/) {
			print $line;
		}
		else {
			last;
		}
	}
	print "\n";
	close GATK;
	open(GATK, $GATK) or die("Cannot open input file ".$GATK."\n");
}
# process non-header
my %varieties; # associates each variety to a column in the VCF file
while (my $line = <GATK>) {
	if ($line =~ /^\#/) {
		if ($line =~ /^\#CHROM/) {
			chomp $line;
			my @fields = split(/\t/, $line);
			for (my $i = 9; $i < @fields; $i++) {
				if (!exists($minCoverage{$fields[$i]})) { die($fields[$i]." not present in min coverage file"); }
				$varieties{$i} = $fields[$i];
			}
		}
		next;
	}
	chomp $line;
	my @fields = split(/\t/, $line);
	my $alternative = $fields[4];
	if ($alternative eq '.') { next; }
	my @alternatives; # multiple variants may be possible
	if ($alternative =~ /,/) { # multiple variants
		@alternatives = split(/,/, $alternative);

	}
	else { # single variant
		$alternatives[0] = $alternative;
	}

	my $chr = $fields[0];
	my $pos = $fields[1];
	my $reference = $fields[3];
	$SNP{$chr}{$pos}{Reference} = $reference;

	my $quality = $fields[5];
	my $filter = $fields[6];
	my $info = $fields[7];
	my $flags = $fields[8];
	my @flags = split(/:/, $flags);

	for (my $i = 9; $i < @fields; $i++) {
		my $values = $fields[$i];
		my $variety = $varieties{$i};

		# parse filter (PASS)
		if (($PASS) and ($filter !~ /PASS/)) {
			$SNP{$chr}{$pos}{$variety}{Variants} = "noPASS";
		}

		# parse quality
		if (($quality ne '.') and ($quality < $QUAL)) {
			$SNP{$chr}{$pos}{$variety}{Variants} = "lowQual";
		}


		# extract flags and corresponding values
		my @values = split(/:/, $values);
		my %flags;
		for (my $index = 0; $index <= $#flags; $index++) {
			$flags{$flags[$index]} = $values[$index];
		}

		if (exists($flags{"GT"})) {
			if ($flags{"GT"} =~ /\./) {
				$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
				$SNP{$chr}{$pos}{$variety}{Coverage} = "0";
			}
			else {
				if ($flags{"GT"} eq "1/0") {
					die("Unexpected GT 1/0 at line ".$line." on variety ".$variety."\n");
				}
				$SNP{$chr}{$pos}{$variety}{GT} = $flags{"GT"};
			}
		}
		else {
			die("Cannot find GT at line ".$line." on variety ".$variety."\n");
		}

#		if (exists($flags{"DP"})) {
#		if ((exists($flags{"DP"})) and ((!exists($SNP{$chr}{$pos}{$variety}{Variants})) or ($SNP{$chr}{$pos}{$variety}{Variants} ne "GTfailed"))) {
		if ((exists($flags{"DP"})) and (!exists($SNP{$chr}{$pos}{$variety}{Variants}))) {
			my $cov = $flags{"DP"};
			$SNP{$chr}{$pos}{$variety}{Coverage} = $cov;
			if ($cov < $minCoverage) {
				$SNP{$chr}{$pos}{$variety}{Variants} = "low_cov";
			}
			else {
				$SNP{$chr}{$pos}{$variety}{GT} =~ /([0123])\/([0123])/;
				if ($1 == $2) { # homozygous SNP
					if ($cov < $minCoverage{$variety} / 2) { # for homozygous SNP we allow half of the minimum coverage in order to recove SNP in hemizygous regions
						$SNP{$chr}{$pos}{$variety}{Variants} = "low_cov";
					}
				}
				else { # heterozygous SNP
					if ($cov < $minCoverage{$variety}) {
						$SNP{$chr}{$pos}{$variety}{Variants} = "low_cov";
					}
				}
				if ($cov > $maxCoverage{$variety}) {
					$SNP{$chr}{$pos}{$variety}{Variants} = "high_cov";
				}
			}
		}
		elsif (!exists($SNP{$chr}{$pos}{$variety}{Variants})) {
			die("Cannot find DP at line ".$line." on variety ".$variety."\n");
		}

		# parse Allele Frequency (AD)
#		if (exists($flags{"AD"})) {
		if ((exists($flags{"AD"})) and (!exists($SNP{$chr}{$pos}{$variety}{Variants}))) {
			my @alleleFreq = split(/,/,$flags{"AD"});
			my $AD_tot = 0;
			for (my $j = 0; $j < @alleleFreq; $j++) {
				$AD_tot += $alleleFreq[$j];
			}
#			if (@alleleFreq != 2) { die("Cannot find 2 Allele Depths in ".$flags{"AD"}." at line ".$line."\n") };
			if (($AD_tot / $SNP{$chr}{$pos}{$variety}{Coverage} < $AD_on_DP_percentage) or ($SNP{$chr}{$pos}{$variety}{Coverage} / $AD_tot < $AD_on_DP_percentage)) {
				$SNP{$chr}{$pos}{$variety}{Variants} = "lowADqual";
			}
		}
		elsif (!exists($SNP{$chr}{$pos}{$variety}{Variants})) {
			die("Cannot find AD at line ".$line." on variety ".$variety."\n");
		}

#		if (exists($flags{"PL"})) {
		if ((exists($flags{"PL"})) and (!exists($SNP{$chr}{$pos}{$variety}{Variants}))) {
			$SNP{$chr}{$pos}{$variety}{PL} = $flags{"PL"};
		}
		elsif (!exists($SNP{$chr}{$pos}{$variety}{Variants})) {
			die("Cannot find PL at line ".$line." on variety ".$variety."\n");
		}

		# GT and PL should be concordant
		if (!exists($SNP{$chr}{$pos}{$variety}{Variants})) {
			my @PL = split(/,/, $SNP{$chr}{$pos}{$variety}{PL});

			# determine minimum PL value (it should be zero!)
			my $minPL = $PL[0];
			my $secondMinPL = $minimumPL;
			if ($PL[0] > 0) {
				$secondMinPL = $PL[0];
			}

			for (my $index = 1; $index < @PL; $index++) {
				if ($PL[$index] < $minPL) { $minPL = $PL[$index]; }
				if (($PL[$index] > 0) and ($PL[$index] < $secondMinPL)) { $secondMinPL = $PL[$index]; }
			}
			if ($minPL != 0) { $SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed"; }
			if ($secondMinPL < $minimumPL) { $SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed"; }
			if ($PL[0] == 0) { # homozygous as reference
				if ($SNP{$chr}{$pos}{$variety}{GT} ne "0/0") {
					$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
				}
			}
			if ($PL[1] == 0) { # heterozygous
				if ($SNP{$chr}{$pos}{$variety}{GT} ne "0/1") {
					$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
				}
			}
			if ($PL[2] == 0) { # homozygous different from reference
				if ($SNP{$chr}{$pos}{$variety}{GT} ne "1/1") {
					$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
				}
			}
			if (@PL >= 6) { # two variants
				if ($PL[3] == 0) { # heterozygous as reference and second variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "0/2") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
				if ($PL[4] == 0) { # heterozygous as first and second variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "1/2") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
				if ($PL[5] == 0) { # homozygous as second variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "2/2") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
			}
			if (@PL == 10) { # 3 variants
				if ($PL[6] == 0) { # heterozygous as reference and third variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "0/3") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
				if ($PL[7] == 0) { # heterozygous as first and third variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "1/3") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
				if ($PL[8] == 0) { # heterozygous as second and third variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "2/3") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
				if ($PL[9] == 0) { # homozygous as third variant
					if ($SNP{$chr}{$pos}{$variety}{GT} ne "3/3") {
						$SNP{$chr}{$pos}{$variety}{Variants} = "GTfailed";
					}
				}
			}
		}

		# check genotypes for unbalanced Minimum Variant Frequency
		if ((exists($flags{"AD"})) and (!exists($SNP{$chr}{$pos}{$variety}{Variants}))) {
			my @alleleFreq = split(/,/,$flags{"AD"});
			if (($SNP{$chr}{$pos}{$variety}{GT} ne "0/0") and ($SNP{$chr}{$pos}{$variety}{GT} ne "1/1") and ($SNP{$chr}{$pos}{$variety}{GT} ne "2/2") and ($SNP{$chr}{$pos}{$variety}{GT} ne "3/3")) {
				if ($SNP{$chr}{$pos}{$variety}{GT} eq "0/1") {
					if ($alleleFreq[1] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "0/0";
					}
					elsif ($alleleFreq[1] / $SNP{$chr}{$pos}{$variety}{Coverage} < $MVF) {
						$SNP{$chr}{$pos}{$variety}{Variants} = "MVF";
					}
				}
				elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "0/2") {
					if ($alleleFreq[2] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "0/0";
					}
					elsif ($alleleFreq[2] / $SNP{$chr}{$pos}{$variety}{Coverage} < $MVF) {
						$SNP{$chr}{$pos}{$variety}{Variants} = "MVF";
					}
				}
				elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "1/2") {
					if ($alleleFreq[1] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "2/2";
					}
					elsif ($alleleFreq[2] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "1/1";
					}
					else {
						my $min = $alleleFreq[1];
						if ($alleleFreq[2] < $min) { $min = $alleleFreq[2]; }
						if ($min / $SNP{$chr}{$pos}{$variety}{Coverage} < $MVF) {
							$SNP{$chr}{$pos}{$variety}{Variants} = "MVF";
						}
					}
				}
				elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "0/3") {
					if ($alleleFreq[3] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "0/0";
					}
					elsif ($alleleFreq[3] / $SNP{$chr}{$pos}{$variety}{Coverage} < $MVF) {
						$SNP{$chr}{$pos}{$variety}{Variants} = "MVF";
					}
				}
				elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "1/3") {
					if ($alleleFreq[1] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "3/3";
					}
					elsif ($alleleFreq[3] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "1/1";
					}
					else {
						my $min = $alleleFreq[1];
						if ($alleleFreq[3] < $min) { $min = $alleleFreq[3]; }
						if ($min / $SNP{$chr}{$pos}{$variety}{Coverage} < $MVF) {
							$SNP{$chr}{$pos}{$variety}{Variants} = "MVF";
						}
					}
				}
				elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "2/3") {
					if ($alleleFreq[2] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "3/3";
					}
					elsif ($alleleFreq[3] == 1) {
						$SNP{$chr}{$pos}{$variety}{GT} = "2/2";
					}
					else {
						my $min = $alleleFreq[2];
						if ($alleleFreq[3] < $min) { $min = $alleleFreq[3]; }
						if ($min / $SNP{$chr}{$pos}{$variety}{Coverage} < $MVF) {
							$SNP{$chr}{$pos}{$variety}{Variants} = "MVF";
						}
					}
				}
			}
		}

		# problematic positions are discarded
		if (!exists($SNP{$chr}{$pos}{$variety}{Variants})) {

			if (!defined($SNP{$chr}{$pos}{$variety}{PL})) {
				die("Cannot find Phred-Likelihood field at chr $chr, position $pos for variety $i\n");
			}
			if ($SNP{$chr}{$pos}{$variety}{GT} eq "0/0") { # homozygous as reference
				$SNP{$chr}{$pos}{$variety}{Variant1} = $reference;
				$SNP{$chr}{$pos}{$variety}{Variant2} = $reference;
				$SNP{$chr}{$pos}{$variety}{Variants} = 1;

			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "0/1") { # heterozygous variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $reference;
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[0];
				$SNP{$chr}{$pos}{$variety}{Variants} = 2;
			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "1/1") { # homozygous variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $alternatives[0];
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[0];
				$SNP{$chr}{$pos}{$variety}{Variants} = 1;
			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "0/2") { # heterozygous as reference and second variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $reference;
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[1];
				$SNP{$chr}{$pos}{$variety}{Variants} = 2;

			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "1/2") { # heterozygous as first and second variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $alternatives[0];
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[1];
				$SNP{$chr}{$pos}{$variety}{Variants} = 2;
			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "2/2") { # homozygous as second variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $alternatives[1];
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[1];
				$SNP{$chr}{$pos}{$variety}{Variants} = 1;
			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "0/3") { # heterozygous as reference and third variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $reference;
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[2];
				$SNP{$chr}{$pos}{$variety}{Variants} = 2;

			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "1/3") { # heterozygous as first and third variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $alternatives[0];
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[2];
				$SNP{$chr}{$pos}{$variety}{Variants} = 2;
			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "2/3") { # heterozygous as second and third variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $alternatives[1];
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[2];
				$SNP{$chr}{$pos}{$variety}{Variants} = 2;
			}
			elsif ($SNP{$chr}{$pos}{$variety}{GT} eq "3/3") { # homozygous as third variant
				$SNP{$chr}{$pos}{$variety}{Variant1} = $alternatives[2];
				$SNP{$chr}{$pos}{$variety}{Variant2} = $alternatives[2];
				$SNP{$chr}{$pos}{$variety}{Variants} = 1;
			}
			else { die("Unknown genotype ".$SNP{$chr}{$pos}{$variety}{GT}."\n"); }
		}
	}
}
close GATK;

# process position by position
foreach my $chr(sort keys %SNP) {
        foreach my $pos (sort {$a <=> $b} keys %{$SNP{$chr}}) {
		my $countVarieties = 0;
		my $countSNP = 0;
		my $Var1;
		my $Var2;
		my $different_genotypes = 0; # to detect identical genotypes in all varieties
		for (my $i=0; $i < @varieties; $i++) {
			my $Variety = $varieties[$i];
			if (exists($SNP{$chr}{$pos}{$Variety}) and ($SNP{$chr}{$pos}{$Variety}{Variants} !~ /multiGenotype|low_cov|high_cov|noPASS|lowQual|lowAlleleFrequency|noGenotype|lowADqual|GTfailed|MVF/)) {
				$countVarieties++;
				if (($SNP{$chr}{$pos}{$Variety}{Variant1} ne $SNP{$chr}{$pos}{Reference})
					or ($SNP{$chr}{$pos}{$Variety}{Variant2} ne $SNP{$chr}{$pos}{Reference})) {
					$countSNP++;
				}

				# check whether all genotypes are identical
				if (defined($Var1)) {
					if ($Var1 ne $SNP{$chr}{$pos}{$Variety}{Variant1}) { $different_genotypes = 1; }
				}
				else {
					$Var1 = $SNP{$chr}{$pos}{$Variety}{Variant1};
				}
				if (defined($Var2)) {
					if ($Var2 ne $SNP{$chr}{$pos}{$Variety}{Variant2}) { $different_genotypes = 1; }
				}
				else {
					$Var2 = $SNP{$chr}{$pos}{$Variety}{Variant2};
				}
			}
		}
		if (($countVarieties >= $minVarieties) and ($countSNP >= $minSNP) and ($different_genotypes)) {
			my $A = 0;
			my $C = 0;
			my $G = 0;
			my $T = 0;

			my $multiGenotype = 0;
			for (my $i=0; $i < @varieties; $i++) {
				my $Variety = $varieties[$i];
				if (exists($SNP{$chr}{$pos}{$Variety})) {
					if ($SNP{$chr}{$pos}{$Variety}{Variants} !~ /multiGenotype|low_cov|high_cov|noPASS|lowQual|lowAlleleFrequency|noGenotype|lowADqual|GTfailed|MVF/) {
						if ($SNP{$chr}{$pos}{$Variety}{Variant1} eq 'A') { $A = 1; }
						elsif ($SNP{$chr}{$pos}{$Variety}{Variant1} eq 'C') { $C = 1; }
						elsif ($SNP{$chr}{$pos}{$Variety}{Variant1} eq 'G') { $G = 1; }
						elsif ($SNP{$chr}{$pos}{$Variety}{Variant1} eq 'T') { $T = 1; }
						if ($SNP{$chr}{$pos}{$Variety}{Variant2} eq 'A') { $A = 1; }
						elsif ($SNP{$chr}{$pos}{$Variety}{Variant2} eq 'C') { $C = 1; }
						elsif ($SNP{$chr}{$pos}{$Variety}{Variant2} eq 'G') { $G = 1; }
						elsif ($SNP{$chr}{$pos}{$Variety}{Variant2} eq 'T') { $T = 1; }
					}
					if ($SNP{$chr}{$pos}{$Variety}{Variants} =~ /multiGenotype/) {
						$multiGenotype = 1;
					}
				}
			}

			if (($A + $C + $G + $T > 1) and ($A + $C + $G + $T <= $maxGenotypes) and (!$multiGenotype)) {
				print $chr."\t".$pos."\t".$SNP{$chr}{$pos}{Reference}.$SNP{$chr}{$pos}{Reference}."\t";
				for (my $i=0; $i < @varieties; $i++) {
					my $Variety = $varieties[$i];
					if (exists($SNP{$chr}{$pos}{$Variety})) {
						if ($SNP{$chr}{$pos}{$Variety}{Variants} !~ /multiGenotype|low_cov|high_cov|noPASS|lowQual|lowAlleleFrequency|noGenotype|lowADqual|GTfailed|MVF/) {
							if ($SNP{$chr}{$pos}{$Variety}{Variant1} eq $SNP{$chr}{$pos}{Reference}) {
								print $SNP{$chr}{$pos}{$Variety}{Variant1}.$SNP{$chr}{$pos}{$Variety}{Variant2}."(".$SNP{$chr}{$pos}{$Variety}{Coverage}.")"."\t";
							}
							elsif ($SNP{$chr}{$pos}{$Variety}{Variant2} eq $SNP{$chr}{$pos}{Reference}) {
								print $SNP{$chr}{$pos}{$Variety}{Variant2}.$SNP{$chr}{$pos}{$Variety}{Variant1}."(".$SNP{$chr}{$pos}{$Variety}{Coverage}.")"."\t";
							}
							elsif ($SNP{$chr}{$pos}{$Variety}{Variant1} le $SNP{$chr}{$pos}{$Variety}{Variant2}) {
								print $SNP{$chr}{$pos}{$Variety}{Variant1}.$SNP{$chr}{$pos}{$Variety}{Variant2}."(".$SNP{$chr}{$pos}{$Variety}{Coverage}.")"."\t";
							}
							else {
								print $SNP{$chr}{$pos}{$Variety}{Variant2}.$SNP{$chr}{$pos}{$Variety}{Variant1}."(".$SNP{$chr}{$pos}{$Variety}{Coverage}.")"."\t";
							}
						}
						else {
							print $SNP{$chr}{$pos}{$Variety}{Variants}."(".$SNP{$chr}{$pos}{$Variety}{Coverage}.")"."\t";
						}
					}
					else {
						print "Undefined(0)"."\t";
					}
				}
				print "\n";
			}
		}

		# delete information of position just processed
		delete($SNP{$chr}{$pos});
	}
}

__END__


=head1 GFF Statistics

SNP_table_vcf_inMemory_PL_GATKtable.pl - Using this script

=head1 SYNOPSIS

perl SNP_table_vcf_inMemory_PL_GATKtable.pl [--GATK <filename>] [--minVarieties <integer>] [--minSNP <integer>] [--coverage <file>] [--minCoverageRatio <float>] [--maxCoverageRatio <float>] [--minCoverage <integer>] [--maxGenotypes <integer>] [--header <integer>] [--PASS <integer>] [--AlleleFrequency <float>] [--QUAL <integer>] [--AD_on_DP_percentage <float>] [--minimumPL <integer>] [--MVF <float>] [--help]

  Options:
    --GATK <filename>		SNP file in VCF format
    --minVarieties <integer>	minimum number of varieties to consider a SNP (default 2)
    --minSNP <integer>		minimum number of varieties exhibting a SNP (default 1)
    --coverage <file>		file with coverage to consider a position for each variety (two columns, variety cov), otherwise rejected
    --minCoverageRatio <float>	minimum coverage compared to average coverage, default 0.5
    --maxCoverageRatio <float>	maximum coverage compared to average coverage, default 1.5
    --minCoverage <integer>	minimum coverage, regardless of individual average coverage, default 5
    --maxGenotypes <integer>	maximum number of genotypes, fastPHASE works only with 2 (default 2)
    --header <integer>		print vcf headers (0=no, 1=yes, default no)
    --PASS <integer>		keep only SNPs with PASS column (0=no, 1=yes, default no)
    --AlleleFrequency <float>	Minimum Variant Frequency for single genotypes of Minimum Allele Frequency for multi-samples (default 0.01)
    --QUAL <integer>		minimum variant quality (Phred scaled), default 0
    --AD_on_DP_percentage <float> minimum ratio between AD and DP values, default 0.8
    --minimumPL <integer>	minimum value of PL for non zero values, default 30
    --MVF <float>		minimum variant frequency for a single sample call, default 0.2 (i.e. 20%)
    --help			print this help message

output on standard output (redirect, if necessary)
