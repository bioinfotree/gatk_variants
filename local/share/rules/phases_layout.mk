# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

phase phase_1

# needed by phase_1
TRF_PARAM := 2 7 7 80 10 50 170
DELETIONS_FASTA := ../../test/all_deletions.fa.gz
DELETIONS_LST := ../../../../../../../../../vigneto/share/pinosio/breakdancer/results/all_deletions.txt