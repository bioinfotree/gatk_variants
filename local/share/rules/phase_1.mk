# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>



# generate links
link.mk:
	FILE_LST=""; \
	WIG_LST=""; \
	COUNTER=0; \
	for FILE in $(UNIQ_REALIGNED_BAM); do \
	  ln -sf $$FILE $$COUNTER.uniq_realigned.bam; \
	  ln -sf "$${FILE%.*}".wig.gz  $$COUNTER.uniq_realigned.wig.gz; \
	  FILE_LST="$$FILE_LST $$COUNTER.uniq_realigned.bam"; \
	  WIG_LST="$$WIG_LST $$COUNTER.uniq_realigned.wig.gz"; \
	  COUNTER=$$[$$COUNTER +1]; \
	done; \
	printf "UNIQ_REALIGNED_BAM_LN := $$FILE_LST\n" >$@; \
	printf "UNIQ_REALIGNED_WIG_LN := $$WIG_LST" >>$@

include link.mk

reference.fasta:
	ln -sf $(REFERENCE) $@

UNIQ_REALIGNED_BAI = $(UNIQ_REALIGNED_BAM_LN:.bam=.bam.bai)

%.uniq_realigned.bam.bai: %.uniq_realigned.bam
	$(call module_loader); \
	samtools index $<


# Unified Genotyper: VARIANT CALL
raw.variants.vcf: $(UNIQ_REALIGNED_BAI)
	!threads
	$(call module_loader); \
	java -Xmx12g -Djava.io.tmpdir=$$PWD/tmp -jar $(GATK) \
	-R $(REFERENCE) \
	-T UnifiedGenotyper \
	$(addprefix -I ,$(basename $^)) \
	--genotype_likelihoods_model BOTH \
	-o $@ \
	--reference_sample_name $(basename $(REFERENCE)) \
	--heterozygosity 0.01 \   * for plants the frequency of variants is much higher than in human *
	-nt $$THREADNUM


# selection of SNPs outside SSR
snps.vcf: raw.variants.vcf
	!threads
	$(call module_loader); \
	java -Xmx12g -Djava.io.tmpdir=$$PWD/tmp -jar $(GATK) \
	-R $(REFERENCE) \
	-T SelectVariants \
	--variant $< \
	-o $@ \
	-selectType SNP \
	-restrictAllelesTo BIALLELIC \
	-nt $$THREADNUM

# chromosome list
chr.lst: reference.fasta
	fasta2tab <$< \
	| cut -f1 >$@

# append chrs list to make variable
chr.lst.mk: chr.lst
	printf "CHR_LST := $$(tr \\n ' ' <$<)" >$@

include chr.lst.mk

CHR_LST_RECODE = $(addsuffix .recode.vcf,$(CHR_LST))

# since the script SNP_table_vcf_inMemory_PL_GATKtable_homozygous.pl needs too much memory,
# we split the original vcf file by chromosomes
%.recode.vcf: snps.vcf
	$(call module_loader); \
	vcftools --vcf $< --chr $* --out $* --recode

# create histogram of coverage including mode and mean of coverage.
# this script is very simple and no matematical and work in empirical way
UNIQ_REALIGNED_WIG = $(UNIQ_REALIGNED_WIG_LN:.wig.gz=.cov.hist)
%.cov.hist: %.wig.gz
	histogram.pl --wig <(zcat <$<) >$@

## N.B. Edit me with the real name of the samples 
samples.cov: $(UNIQ_REALIGNED_WIG)
	$(warning ===>[WARNING!]<=== REMEBER TO EDIT $@ WITH THE REAL NAME OF THE SAMPLES AND TO CONTROL THE MODE)
	grep 'Mode (no zero)' $^ \
	| tr ':' \\t \
	| cut -f 1,3 >$@

CHR_LST_TABLE = $(addsuffix .table,$(CHR_LST))


%.table: %.recode.vcf samples.cov
	if [ -s $< ]; then \
		SNP_table_vcf_inMemory_PL_GATKtable_homozygous.pl \
		--GATK $< \
		--minVarieties 1 \
		--minSNP 1 \
		--coverage $^2 \   * moda of coverage *
		--minCoverageRatio 0.5 \
		--maxCoverageRatio 1.5 \
		--minCoverage 5 \
		--maxGenotypes 2 \
		--QUAL 30 \
		--AD_on_DP_percentage 0.8 \
		--AlleleFrequency 0.4 \   * to remove the background noise of heterozygosity in rpv3 *
		--minimumPL 20 >$@; \
	fi


total.tables: $(CHR_LST_TABLE)
	>$@; \
	for FILE in $^; do \
		[ -s "$$FILE" ] && bawk '!/^[$$,\#+]/ { print $$0 }' <$$FILE >>$@; \
	done

# calculate heterozygosity between 2 samples and write them in
# .seg format: http://www.broadinstitute.org/software/igv/SEG
#
# add track lanes. See here for more informations about tags: http://www.broadinstitute.org/igv/TrackLine
# see here for more informationsa about about graphs and data ranges: https://www.broadinstitute.org/software/igv/ChangeDataDisplay#DataRange 
heterozygosity.3-%.seg: total.tables
	heterozygosity.R -w 100000 -o scatterplot.$(basename $(basename $@)).3-$*.pdf 3 $* <$< \
	| sed '1s/^/\#track type="Other" name="het_V3_V$*" color="50,150,255" graphType="bar" viewLimits="0.0:0.01" scaleType="linear"\n/' >$@



.PHONY: test
test:
	@echo $(CHR_LST_RECODE)


# This should be the default target.
ALL   +=  samples.cov \
	  reference.fasta \
	  $(UNIQ_REALIGNED_BAM_LN) \
	  $(UNIQ_REALIGNED_WIG_LN) \
	  $(UNIQ_REALIGNED_BAI) \
	  raw.variants.vcf \
	  snps.vcf \
	  chr.lst \
	  $(CHR_LST_RECODE) \
	  $(UNIQ_REALIGNED_WIG) \
	  $(CHR_LST_TABLE) \
	  total.tables \
	  heterozygosity.3-4.seg \
	  heterozygosity.3-5.seg \
	  heterozygosity.3-6.seg \
	  link.mk \
	  chr.lst.mk


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=



# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += tmp \
	 $(wildcard *.idx) \
	 $(wildcard *.vcfidx) \
	 $(addsuffix .log,$(CHR_LST))